# Connection Counter

This script was made with the user of Bard, an AI product from Google.

## Script Description

It lists the number of connections made by source IPs to different destination IPs.
